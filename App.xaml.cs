﻿using MVVMTask.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMTask
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new StartPage());
        }

        async protected override void OnStart()
        {
            await MainPage.DisplayAlert("Alert", "App has been started!", "OK");
        }

        protected override void OnSleep()
        {
        }

        async protected override void OnResume()
        {
            await MainPage.DisplayAlert("Alert", "App resumed now!", "OK");
        }
    }
}
