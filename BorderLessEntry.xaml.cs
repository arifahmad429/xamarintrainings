﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BorderLessEntry : ContentPage
    {
        public BorderLessEntry()
        {
            InitializeComponent();
        }
    }
}