﻿using ActivityLoader.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ActivityLoader.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Chips
    {
       // public static readonly BindableProperty ItemSourceProperty = BindableProperty.Create(nameof(ItemSource), typeof(string), typeof(Chips), default(string), propertyChanged: HandleItemSourcePropertyChanged);
        public static readonly BindableProperty PickerItemsSourceProperty = BindableProperty.Create("PickerItemSource", typeof(IList), typeof(Chips));
        public Chips()
        {
            InitializeComponent();
        }

        //public string ItemSource
        //{
        //    get => (string)GetValue(ItemSourceProperty);
        //    set => SetValue(ItemSourceProperty, value);
        //}

        public IList PickerItemSource
        {
            get => (IList)GetValue(PickerItemsSourceProperty);
            set => SetValue(PickerItemsSourceProperty, value);
        }




        //private static void HandleItemSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        //{
        //    if (!(bindable is Chips chips) || !(newValue is string itemSource) || string.IsNullOrWhiteSpace(itemSource))
        //    {
        //        return;
        //    }

        //    BindableLayout.SetItemsSource(chips.ListBinding, itemSource);
        //}

    }
}