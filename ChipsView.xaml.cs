﻿using ActivityLoader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ActivityLoader.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChipsView
    {
        public static readonly BindableProperty ItemSourceProperty = BindableProperty.Create(nameof(ItemSource), typeof(string), typeof(ChipsView), default(string), propertyChanged: HandleItemSourcePropertyChanged);
        public static readonly BindableProperty ButtonBackgroundProperty = BindableProperty.Create(nameof(ButtonBackgroundColor), typeof(Color), typeof(ChipsView), default(Color), propertyChanged: HandleButtonBackgroundPropertyChanged);
        public static readonly BindableProperty ListTextColorProperty = BindableProperty.Create(nameof(ListTextColor), typeof(Color), typeof(ChipsView), default(Color), propertyChanged: HandleListTextColorPropertyChanged);
        public static readonly BindableProperty FrameBackgroundProperty = BindableProperty.Create(nameof(FrameBackgroundColor), typeof(Color), typeof(ChipsView), default(Color), propertyChanged: HandleFrameBackgroundPropertyChanged);
        public static readonly BindableProperty TextBackgroundProperty = BindableProperty.Create(nameof(TextBackgroundColor), typeof(Color), typeof(ChipsView), default(Color), propertyChanged: HandleTextBackgroundPropertyChanged);
        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(ChipsView), default(string), propertyChanged: HandleTextPropertyChanged);
        public static readonly BindableProperty TextFontSizeProperty = BindableProperty.Create(nameof(TextFontSize), typeof(double), typeof(ChipsView), default(double), propertyChanged: HandleTextFontSizePropertyChanged);
        public static readonly BindableProperty ChipsBorderColorProperty = BindableProperty.Create(nameof(ChipsBorderColor), typeof(Color), typeof(ChipsView), default(Color), propertyChanged: HandleChipsBorderPropertyChanged);


        public ChipsView()
        {
            InitializeComponent();
        }

        public string ItemSource
        {
            get => (string)GetValue(ItemSourceProperty);
            set => SetValue(ItemSourceProperty, value);
        }

        public Color ButtonBackgroundColor
        {
            get => (Color)GetValue(ButtonBackgroundProperty);
            set => SetValue(ButtonBackgroundProperty, value);
        }

        public Color ListTextColor
        {
            get => (Color)GetValue(ListTextColorProperty);
            set => SetValue(ListTextColorProperty, value);
        }

        public Color FrameBackgroundColor
        {
            get => (Color)GetValue(FrameBackgroundProperty);
            set => SetValue(FrameBackgroundProperty, value);
        }

        public Color TextBackgroundColor
        {
            get => (Color)GetValue(TextBackgroundProperty);
            set => SetValue(TextBackgroundProperty, value);
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public double TextFontSize
        {
            get => (double)GetValue(TextFontSizeProperty);
            set => SetValue(TextFontSizeProperty, value);
        }

        public Color ChipsBorderColor
        {
            get => (Color)GetValue(ChipsBorderColorProperty);
            set => SetValue(ChipsBorderColorProperty, value);
        }

        private static void HandleItemSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bindable is ChipsView chips) || !(newValue is string itemSource) || string.IsNullOrWhiteSpace(itemSource))
            {
                return;
            }

            BindableLayout.SetItemsSource(chips.SourceBinding, itemSource);
        }

        private static void HandleButtonBackgroundPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bindable is ChipsView chips) || !(newValue is Color buttonBackgroundColor))
            {
                return;
            }

            chips.ButtonBackground.BackgroundColor = buttonBackgroundColor;
        }

        private static void HandleListTextColorPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if(!(bindable is ChipsView chips) || !(newValue is Color listTextColor))
            {
                return;
            }

            chips.CellDataView.TextColor = listTextColor;
        }

        private static void HandleFrameBackgroundPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if(!(bindable is ChipsView chips) || !(newValue is Color frameBackgroundColor))
            {
                return;
            }

            chips.FrameProperty.BackgroundColor = frameBackgroundColor;
        }

        private static void HandleTextBackgroundPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if(!(bindable is ChipsView chips) || !(newValue is Color textBackgroundColor))
            {
                return;
            }

            chips.CellDataView.BackgroundColor = textBackgroundColor;
        }

        private static void HandleTextPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (!(bindable is ChipsView chips) || !(newValue is string text) || string.IsNullOrWhiteSpace(text))
            {
                return;
            }

            chips.CellDataView.Text = text;
        }

        private static void HandleTextFontSizePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if(!(bindable is ChipsView chips) || !(newValue is double textFontSize))
            {
                return;
            }

            chips.CellDataView.FontSize = textFontSize;
        }

        private static void HandleChipsBorderPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if(!(bindable is ChipsView chips) || !(newValue is Color chipsBorderColor))
            {
                return;
            }

            chips.FrameProperty.BorderColor = chipsBorderColor;
        }

    }
}