﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMTask.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContentViews : ContentPage
    {
        public ContentViews()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            homecheck.Content = new HomeContent();
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
            homecheck.Content = new GalleryContent();
        }

        private void Button_Clicked_2(object sender, EventArgs e)
        {
            homecheck.Content = new ContactContent();
        }

        private void Button_Clicked_3(object sender, EventArgs e)
        {
            homecheck.Content = new AboutContent();
        }
    }
}