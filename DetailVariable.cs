﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVVMTask.ViewModels
{
    class DetailVariable
    {
        public string profileUrl
        {
            get
            {
                return "ProfilePicture.PNG";
            }
        }
        public string profileStatus
        {
            get
            {
                return "Profile created on:";
            }
        }

        public string profileDate
        {
            get
            {
                return " 21 Apr 2020";
            }
        }
        public string accountNumber
        {
            get
            {
                return "3249593485";

            }
        }

        public string empName
        { 
            get
            {
                return "Arif Ahmad";   
            } 
        }

        public string type
        {
            get
            {
                return "Trainee Engineer";
            }
        }

        public string email
        {
            get
            {
                return "arif.ahmad@successive.tech";
            }
        }

        public string phoneNumber
        {
            get
            {
                return "+91-7007864538";
            }
        }
        public string location
        {
            get
            {
                return "Noida, Uttar Pradesh";
            }
        }

    }
}
