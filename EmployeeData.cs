﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace MVVMTask
{
    public class EmployeeData
    {
        public static ObservableCollection<Employee> GetList()
        {
            var l = new ObservableCollection<Employee>();

            l.Add(new Employee() { FullName = "Arif Ahmad", Department = "Xamarin Trainee" });
            l.Add(new Employee() { FullName = "Akash Gaur", Department = "DotNet Developer" });
            l.Add(new Employee() { FullName = "Prabhat Kumar", Department = "Xamarin Trainee" });
            l.Add(new Employee() { FullName = "Shivani Mangla", Department = "Xamarin Developer" });
            l.Add(new Employee() { FullName = "Kartik Parashar", Department = "Xamarin Developer" });
            l.Add(new Employee() { FullName = "Anand Srivastava", Department = "DotNet Developer" });
            l.Add(new Employee() { FullName = "Tarun Singh", Department = "DotNet Developer" });
            l.Add(new Employee() { FullName = "Sid Pandey", Department = "CEO" });
            l.Add(new Employee() { FullName = "Jyoti Sagar", Department = "HR" });
            l.Add(new Employee() { FullName = "Manisha Rawat", Department = "HR Specialist" });
            l.Add(new Employee() { FullName = "Aakriti Gupta", Department = "Associative HR" });
            l.Add(new Employee() { FullName = "Saurabh Shukla", Department = "Database" });
            l.Add(new Employee() { FullName = "Zahid Azmi", Department = "IT Manager" });
            l.Add(new Employee() { FullName = "Ambuj Hardeya", Department = "IT Specialist" });
            l.Add(new Employee() { FullName = "Priya Goel", Department = "Testing Department" });
            l.Add(new Employee() { FullName = "ShikharDeep Bhatnagar", Department = "Xamarin Developer" });
            l.Add(new Employee() { FullName = "Swapnil Kumar", Department = "Android Developer" });
            l.Add(new Employee() { FullName = "Skand Swami", Department = "iOS Developer" });
            l.Add(new Employee() { FullName = "Shashank Singh", Department = "CrossPlatform Developer" });
            l.Add(new Employee() { FullName = "Ayush Aggarwal", Department = "Testing Department" });

            return l;
        }
    }
}
