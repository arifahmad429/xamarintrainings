﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMTask
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EmployeeDirectoryView : ContentPage
    {
        public ObservableCollection<Employee> empList = EmployeeData.GetList();

        public event PropertyChangedEventHandler PropertyChanged;

        public EmployeeDirectoryView()
        {
            InitializeComponent();

            BindingContext = new EmployeeData();

            empListView.ItemsSource = empList;

            //var groupedData =
            //    empList.OrderBy(e => e.FullName)
            //        .GroupBy(e => e.FullName[0].ToString())
            //        .Select(e => new ObservableGroupCollection<string, Employee>(e))
            //        .ToList();

            //BindingContext = new ObservableCollection<ObservableGroupCollection<string, Employee>>(groupedData);
        }

         
        private void Button_Clicked(object sender, EventArgs e)
        {
            var NewEmp = AddName.Text;
            var NewDept = AddNewDepartment.Text;

            empList.Add(new Employee { FullName = NewEmp, Department = NewDept });

            DisplayAlert("Message", "Employee added Successfully!", "OK");
        }


        private void OnRemove(object sender, EventArgs e)
        {
            DisplayAlert("Message", "Employee has been removed!", "OK");

            var removeEmployee = (sender as MenuItem).CommandParameter as Employee;

            empList.Remove(removeEmployee);
        }
    }
}