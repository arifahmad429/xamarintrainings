﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


namespace XamarinProjectTasks
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        const string email = "arif";
        const string pass = "123";
        public MainPage()
        {
            InitializeComponent();
        }

        
        private void Button_Clicked(object sender, EventArgs e)
        {
      
            if (Email.Text == email && Password.Text == pass)
            {
                DisplayAlert("Alert", "Login Successful", "OK");
            }
            else
            {
                DisplayAlert("Alert", "Invalid Email or Password", "OK");
            }
        }
    }
}
