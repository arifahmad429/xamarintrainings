﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace MVVMTask
{
    public class ObservableCollection<K, T> : ObservableCollection<T>
    {
        private readonly K _key;

        public ObservableCollection(IGrouping<K, T> group)
            : base(group)
        {
            _key = group.Key;
        }

        public K Key
        {
            get { return _key; }
        }
    }
}
