﻿using MVVMTask.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace MVVMTask.ViewModels
{
    class LoginViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler Show;
        
        private string email;
        public string Email
        {
            get
            { 
               return email; 
            }
            set
            {
                email = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Email"));
            }
        }

        private string password;
        public string Password
        {
            get 
            {
                return password; 
            }
            set
            {
                password = value;
                PropertyChanged(this, new PropertyChangedEventArgs("Password"));
            }
        }
        public Command LoginCommand
        {
            get
            {
                return new Command(Login);
            }
        }

        private void Login()
        {
            string email = "arif@gmail.com";
            string pass = "123";

            var inputEmail = "" + Email;
            Regex regex = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
            bool isValid = regex.IsMatch((inputEmail.ToString()).Trim());

            if (!isValid)
            {
                Show?.Invoke("Please enter the valid email", null);
            }

            else if (string.IsNullOrEmpty(Email) && string.IsNullOrEmpty(Password))
            {
                Show?.Invoke("Input field must required!", null);
            }
            else
            {
                if (Email == email && Password == pass)
                {
                    Show?.Invoke("Welcome to Successive!", null);

                    App.Current.MainPage.Navigation.PushAsync(new View.WelcomePage());
                }
                else if (Password == null)
                {
                    Show?.Invoke("Please enter Password", null);
                }
                else
                    Show?.Invoke("Please enter correct Email or Password", null);
            }
        }
    }
}
