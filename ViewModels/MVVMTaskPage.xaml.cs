﻿using MVVMTask.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace MVVMTask.View
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MVVMTaskPage : ContentPage
    {
        LoginViewModel obj = new LoginViewModel();
        public MVVMTaskPage()
        {
             InitializeComponent();
            obj.Show += Model_Show;
            BindingContext = obj;
        }

        private void Model_Show(object sender, EventArgs e)
        {
            var str = (string)sender;
            App.Current.MainPage.DisplayAlert("Error", str, "OK");
        }
    }
}
