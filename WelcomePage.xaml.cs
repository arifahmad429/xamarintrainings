﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MVVMTask.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WelcomePage : MasterDetailPage
    {
        public WelcomePage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void Handle_Clicked(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new Tabbed());
        }

        private void Handle_Clicked2(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new Carousel());
        }

        private void Handle_Clicked3(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new ContentViews());
        }

        private void Handle_Clicked4(object sender, EventArgs e)
        {
            Detail = new NavigationPage(new EmployeeDirectoryView());
        }
    }
}