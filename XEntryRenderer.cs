﻿using Xamarin.Forms;
using MVVMTask.Controls;
using Xamarin.Forms.Platform.Android;
using Android.Graphics.Drawables;
using MVVMTask.Droid.ControlHelpers;

#pragma warning disable CS0612 // Type or member is obsolete
[assembly: ExportRenderer(typeof(XEntry), typeof(XEntryRenderer))]
#pragma warning restore CS0612 // Type or member is obsolete
namespace MVVMTask.Droid.ControlHelpers
{
    [System.Obsolete]
    public class XEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control != null)
            {
                Control.Background = new ColorDrawable(Android.Graphics.Color.Transparent);
            }
        }
    }
}